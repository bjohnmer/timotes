<?php
/*
Template Name: Inicio
*/
get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
    
      <section id="noticias-recientes">
        <article class="wide">
          
        </article>
        <article class="thin">
          
        </article>
      </section>
      <section id="otros" class="full">
        <article class="third"></article>
        <article class="third"></article>
        <article class="third"></article>
      </section>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
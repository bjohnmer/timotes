<?php 

if ( ! function_exists( 'helloAlcaldia' ) ) 
{
    function helloAlcaldia() 
    {
      echo "<!-- Hello Alcaldía -->";    
    }
}


/**
 * Registers two widget areas.
 *
 * @since Twenty Thirteen 1.0
 *
 * @return void
 */
function alcaldia_widgets_init() {
  register_sidebar( array(
    'name'          => __( 'Third Widget Area', 'twentythirteen' ),
    'id'            => 'sidebar-3',
    'description'   => __( 'Pié de página.', 'twentythirteen' ),
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ) );
}
add_action( 'widgets_init', 'alcaldia_widgets_init' );

// add_action( 'wp_head', 'helloAlcaldia' );

?>